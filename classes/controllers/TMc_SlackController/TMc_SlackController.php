<?php

/**
 * Class TMc_SlackController
 */
class TMc_SlackController extends TSc_ModuleController
{
	/**
	 * TSc_PagesController constructor.
	 * @param int|string|TSm_Module $module
	 */
	public function __construct($module)
	{
		parent::__construct($module);
		
	}

	/**
	 * Defines the URL targets
	 */
	public function defineURLTargets()
	{
		// Redirect to Settings
		$target = TSm_ModuleURLTarget::init( '');
		$target->setNextURLTarget('settings');
		$this->addModuleURLTarget($target);

		$item = TSm_ModuleURLTarget::init( 'test-default-webhook');
		$item->setModelName('TMm_SlackClient');
		$item->setModelActionMethod('testDefaultWebhook()');
		$item->setNextURLTarget('referrer');
		$item->setTitle('Test Default');
		$this->addModuleURLTarget($item);

	}
	


}
?>