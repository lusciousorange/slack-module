<?php
class TMv_SlackSettingsForm extends TSv_ModuleSettingsForm
{
	public function __construct($module)
	{	
		parent::__construct($module);
		
	}
	
	public function configureFormElements()
	{
		$field = new TCv_FormItem_TextField('webhook_url', 'Incoming Webhook URL');
		$field->setHelpText('The required webhook in order for Slack connectivity to work. For more information, 
		learn about <a href="https://my.slack.com/services/new/incoming-webhook">Slack Incoming Webhooks</a>. ');
		$this->attachView($field);


		$field = new TCv_FormItem_HTML('webhook_test', 'Test');
		$field->addText('<a href="/admin/slack/do/test-default-webhook/">Test Webhook</a>');
		$this->attachView($field);

	}
	

}
?>