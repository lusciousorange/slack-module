<?php

/**
 * Class TMv_SlackUserForm
 */
class TMv_SlackUserForm extends TMv_UserForm
{
	/**
	 * TMv_UserForm constructor.
	 * @param string|TMm_User $model
	 */
	public function __construct($model)
	{	
		parent::__construct($model);
		
	}

	/**
	 * Configures the form items for the user form.
	 */
	public function configureFormElements()
	{
		$field = new TCv_FormItem_TextField('slack_name', 'Slack Name @');
		$field->setIsRequired();
		$this->attachView($field );
		

	}


}