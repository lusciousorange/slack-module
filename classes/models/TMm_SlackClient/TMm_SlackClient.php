<?php

/**
 * Class TMm_SlackClient
 *
 * A wrapper on the main Client class that allows for easy auto-loading but also console integration
 */
class TMm_SlackClient extends TCm_Model
{
	protected $incoming_webhook_api = false;
	protected $payload = array('text' => '');
	protected $attachments = false;


	/**
	 *
	 * Instantiate a new Client.
	 *
	 */
	public function __construct()
	{
		$this->setIncomingWebhookURL(TC_getModuleConfig('slack', 'webhook_url'));

		parent::__construct('slack_client');
	}

	public function resetMessage()
	{
		$this->payload = array('text' => '');
		$this->attachments = false;
	}

	/**
	 * Sets the incoming webhook url
	 * @param string $webhook_url
	 */
	public function setIncomingWebhookURL($webhook_url)
	{
		$this->incoming_webhook_api = $webhook_url;
		$this->addConsoleDebug($webhook_url);
	}

	/**
	 * Cleans the provided text to ensure that the &, < and > are escaped.
	 * @param string $text
	 * @return string
	 */
	public function cleanTextForSlack($text)
	{
		$text = htmlentities($text, ENT_NOQUOTES);
		return $text;
	}

	/**
	 * Generates a link string which includes text and a URL
	 * @param string $text
	 * @param string $link The link relevant to the site root
	 * @return string
	 */
	public function generateLinkText($text, $link)
	{
		$string = " <";
		$string .= $this->fullDomainName().$link;
		$string .= '|';
		$string .= $this->cleanTextForSlack($text);
		$string .= '>';

		return $string;
	}

	/**
	 * Tests the default web hook if possible
	 */
 	public function testDefaultWebhook()
	{
		//$web_hook = TC_getModuleConfig('slack', 'webhook_url');
		$message = "Testing Webhook and other stuff sent from <".$_SERVER['HTTP_REFERER']."|Tungsten>";
		$this->addText($message);
		$success = $this->sendIncomingWebhook();

		if(!$success)
		{
			TC_message('Webhook Unsuccessful', false);
		}
		else
		{
			TC_message('Webhook Successful', true);
		}

	}

	/**
	 * Adds text to the payload
	 *
	 * @param string $text
	 */
	public function addText($text)
	{
		$this->payload['text'] .= $text;
	}

	/**
	 * Adds an attachment with the provided values
	 *
	 * @param array $values
	 */
	public function addAttachment($values)
	{
		if($this->attachments === false)
		{
			$this->attachments = array();
		}
		$this->attachments[] = $values;
	}

	/**
	 * Parses a user and returns the code for slack if it's found
	 *
	 * @param TMm_User $user
	 * @return string
	 */
	public function slackCodeForUser($user)
	{

		$slack_name = $user->valueForProperty('slack_name');
		if($slack_name != '')
		{
			return '<@'.$slack_name.'>';
		}
		return $this->cleanTextForSlack($user->fullName());
	}


	/**
	 * Sends an incoming webhook to the endpoint
	 * @return mixed
	 */
	public function sendIncomingWebhook()
	{
		//$message, $icon = ":longbox:"//
		if($this->incoming_webhook_api === false)
		{
			$this->addConsoleError('No incoming webhook url is set');
			return;
		}

		if($this->attachments)
		{
			$this->payload['attachments'] = $this->attachments;
		}


		$data_param = "payload=" . urlencode(json_encode($this->payload));

		$ch = curl_init($this->incoming_webhook_api);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_param);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		curl_close($ch);

		$success = $result == 'ok';
		$this->payload['json'] = json_encode($this->payload,JSON_UNESCAPED_UNICODE);
		$this->payload['response'] = $result;

		$this->addConsoleMessageWithType($this->incoming_webhook_api, TSm_ConsoleAPICall, !$success, $this->payload);

		// Clears out anything that was sent
		$this->resetMessage();

		return $success;
	}



}

?>