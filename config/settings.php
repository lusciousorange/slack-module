<?php
	$module_title = 'Slack'; // The name displayed for the Module
	$version = "1.0.0"; // the current version of this module
	$min_system_version = '8.7.0';// the earliest version that this module will work with
	$show_in_menu = false;
	$is_public = true;

	// ICON FONT LIBRARY CODE
	// Every module can have a single icon, which is limited to using a code from a font library. The available list of font libraries is outlined below. In each case, you must provide a valid icon value and the system will handle any processing to ensure it is presetned properly.
	// Font Awesome - fortawesome.github.io/Font-Awesome/icons/
	$icon_library_code = 'fab fa-slack';

	// MODEL NAME
	// The class name for the primary model for this module. Most modules have on primary class that is represented. False otherwise. 
	//$model_name = '';
	
	// SECONDARY MODEL NAMES
	// Some complex modules may have secondary models which are also managed within it. This is common for modules that have associated values to be controlled which don't necesitate a separate module. In those instances, those models must also be defined since they are used throughout the system including in the auto-generation of module URL targets in the module controller. For any secondary model, provide a key-array pair. The key must be the model name. The array value must have settings for the "url_target_name_prefix" [string], and "init_url_targets" [boolean] which indicates if the controller should automatically initialize the URL targets. If you manually generate URL targets, make sure to use the same prefix as defined in this setting.
	$model_names_secondary = array();
	
	// MODULE CONTROLLER CLASS
	// The controller class that defines the url targets for this module. Many modules will require a custom module controller, however the most basic module will default to using the TSc_ModuleController.php class. If a custom controller is not needed, comment out the line below.
	$controller_class = 'TMc_SlackController';
	
	// MODULE SETTINGS VARIABLE FORM CLASS
	// If a module has settings that should be defined, then this form view should be created and setup to define them. The Pages module and System module both use module settings and are good examples of how they work. The form extends the TSv_ModuleSettingsForm class. Otherwise, comment it out. 
	$variables_form_class = 'TMv_SlackSettingsForm';
	
	

?>